## Html reflection of some mathematical symmetries

For instance if x is 1 and n is 1..5
where n is 1..n and x is big integer

symmetry result for (x*10^n + x*10^(n-1) )^2

        1
       11
       111 
      1111        
     111111 
   
will produce following result:    

        1
       121
      12321 
     1234321
    123454321


Natural number produces unique result tree.
[Try it out here](http://rberko.com/symmo)

Screenshot:
![Example](example.jpg)
