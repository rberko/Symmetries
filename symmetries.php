<!doctype html>
<html itemtype="http://schema.org/WebPage">
    <head>
        <title>Numeric Symmetries <?php echo 'PHP version: ' . phpversion(); ?> </title>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="resources/css/stries.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!--<script src="http://peterolson.github.com/BigInteger.js/BigInteger.min.js"></script> -->
        <script src="resources/js/BigInteger.min.js"></script>
        <script src="resources/js/stries.js"></script>
    </head>        
<body>
    <?php
        #include("resources/php/count.php");
        #include("../tmp/php/count.php");
        include(dirname(__FILE__)."/../php/count.php");
    ?>    
    <div class="content">
        <div class="header">
            <span class="rightHeader"><a href="https://gitlab.com/rberko/Symmetries.git" target="_blank">GitLab Project</a></span>
            <p id="header">Enter values (natural numbers)</p>
        </div>
        <div class="inputCont">
                <div><input class="c-numeric" id="mainValue" type="text" value="1">&nbsp; Enter natural number x</input></div>
                <!-- TODO enter limitation or at least warning on values, symmetry tree could  grow wildly -->
                <div><input class="c-numeric" id="powerCount" type="text" value="15">&nbsp; Enter time of power repetitions  (x*10<sup>n</sup> + x*10<sup>n-1</sup>) <sup>2</sup></input> </div>
                <div><input id="colorCB" type="checkbox" value="Colored"  onmyClick="calc(this)">Color result</input></div>              
        </div>
        <div class="action_buttons">            
            <div><input id="calcBtn" type="button" onclick="calc(this)" value="Calculate"></input></div>
            <div><input id="clearBtn" type="button" onclick="clearResults(this)" value="Clear"></input></div>
        </div>

        <div id="mainContainer" class="mc" class="main">
        </div>
    </div>
</body>
<script>
    let  nwords = ['zero','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen ', 'twenty '];
    let  tldata = {
        valx: 'x',
        valxpower: 'x<sup>2</sup>',
        valsum:'Sum',
        valLength:'Length',
        sumDif:'Sum Diff'
    }

    let latinAlphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','y','z']

    //latin characters to num
    function strToNumber(str)
    {
        let res=''
        for (let i=0; i < str.length; i++) { 
            console.log(str.charAt(i)); 
            console.log('num:' + (latinAlphabet.indexOf(str.charAt(i).toLowerCase())+1)); 
            res=res+ (latinAlphabet.indexOf(str.charAt(i).toLowerCase())+1)
        }
        console.log('result str representation: ' + res)
        return res
    }

    //get word representation for a numeric character
    function numToWord(numChar) {
        return nwords[numChar]
    }
    //check if output needs to be colored
    function coloredResult(){
        return document.getElementById('colorCB').checked
    }
   
    /**    clear table
    */
    function clearResults(el){
        let div = document.getElementById("mainContainer")
        while (div.firstChild) {
                div.removeChild(div.firstChild);
            }
            //console.log('cleared')
    }

    /* color numeric value going through every character one by one */
    function colorNumber(numValue, tryFull){
        
            let val='',i=0
            //val=this.powerVal
            //  console.log('powerVal:' + this.powerVal )
            //  console.log('l:' + this.powerVal.toString().length )
            if (coloredResult())
            {
                if (tryFull && numToWord(numValue))
                {
                    val =`<span class='${numToWord(numValue)} digit'>${numValue}</span>`
                }
                else
                {
                    while ( i<numValue.toString().length)
                    {
                        let x=numValue.toString().charAt(i)
                        val = val + `<span class='${numToWord(x)} digit'>${x}</span>`
                        i+=1
                    }
                }
            }
            else
            {
                val=numValue.toString()
            }    
            return  val        
    }

    /* generate result table */
    function createTbl(array)
    {
        let div = document.getElementById("mainContainer")
        clearResults(null)

        let tbl = document.createElement("table"), 
            tBody = document.createElement("tbody"),
            th,x=0,
            thRow=document.createElement('tr')

        th = document.createElement('th')
        $(th).text('No');
        thRow.appendChild(th)

        th = document.createElement('th')
        $(th).text(tldata.valx);
        thRow.appendChild(th)

        th = document.createElement('th')
        $(th).html(tldata.valxpower);
        thRow.appendChild(th)

        th = document.createElement('th')
        $(th).html(tldata.valLength);
        thRow.appendChild(th)

        th = document.createElement('th')
        $(th).html(tldata.valsum);
        thRow.appendChild(th)

        th = document.createElement('th')
        $(th).html(tldata.sumDif);
        thRow.appendChild(th)


        tbl.appendChild(thRow)
        tbl.appendChild(tBody)
        div.appendChild(tbl)
        tbl.appendChild(tBody)
        prevLength=0
        array.forEach(function(element)
        {
            //console.log('adding', element.powerVal)
            let td, tr = document.createElement("tr")
            x+=1
            td = document.createElement("td");
            td.className = 'oNo'
            td.innerHTML = x.toString()
            tBody.appendChild(tr).appendChild(td)
            //x value
            td = document.createElement("td");
            td.className = 'oVal'
            td.innerHTML=colorNumber(element.value.toString())
            tBody.appendChild(tr).appendChild(td)
            //power value span
            td = document.createElement("td");
            td.className = 'pVal'
            td.innerHTML= colorNumber(element.powerVal.toString()) //element.getPowerValueSpan()
            tBody.appendChild(tr).appendChild(td)
            //length
            td = document.createElement("td");
            td.innerHTML=element.powerVal.toString().length
            tBody.appendChild(tr).appendChild(td)
            //sum
            td = document.createElement("td");
            td.innerHTML=element.getSum().toString()
            tBody.appendChild(tr).appendChild(td)

            //sum difference from previous element
            td = document.createElement("td");
            td.innerHTML=  colorNumber(element.getSum().minus(prevLength).toString(),true)
            tBody.appendChild(tr).appendChild(td)
            prevLength=element.getSum()
        }
        )
    }

    /* 
        main function which calculates result generate table...
    */
    function calc(el) {
        try {
            let ar = [],val = [],
                x = 2, y = 9, z = 0, maxl,
                r = bigInt(0) 
            
            x = document.getElementById("mainValue").value
            y = document.getElementById("powerCount").value

            while (z < y) {
                r=bigInt(x).times(bigInt(10).pow(z)).plus(r)
                let rPow=bigInt(r).pow(2)
                //console.log('r',r)
                //console.log('rPow',rPow)                
                let myValx = new myValue(r, rPow);
                ar.push(myValx)
                z += 1;
            }

            let lastVal = ar[ar.length - 1]
            maxl = lastVal.powerVal.toString().length

            // create table
            createTbl(ar)
        }
        catch (e) {
            console.error(e)
        }
    }

    // class for value representation
    class myValue {
        constructor(value, powerVal) {
            this.value = value;
            this.powerVal = powerVal;
        }
         
         getSum(){
             let i=0,val=bigInt(0) 
             while ( i<this.powerVal.toString().length)
                {
                    let x=this.powerVal.toString().charAt(i)
                    val = bigInt(val).plus(x)
                    i++
                }   
                return val         
        }
         
         getPowerValueSpan(){
           return colorNumber(this.powerVal.toString())
        }
        
    }

    //calculate on init from default values
    $( document ).ready(function() {
        calc(null)
});
</script>

</html>