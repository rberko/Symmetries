"use strict"
function numeric(e){
    // console.log("numeric start");
    // console.log("key code", e.keyCode);
    // console.log("key code", e.key);
    let  numRegEx = new RegExp('^[0-9]+$');
    if (!numRegEx.test(e.key)){
        e.preventDefault();
    }
}
document.addEventListener('DOMContentLoaded', function() {
    $('.c-numeric').bind('keypress',numeric);
 });
